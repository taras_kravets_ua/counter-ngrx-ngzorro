import { createAction } from '@ngrx/store';
import { CounterActionTypes } from './actionsTypes';

export const increaseCounter = createAction(
  CounterActionTypes.INCREASE_COUNTER,
);

export const decreaseCounter = createAction(
  CounterActionTypes.DECREASE_COUNTER,
);

export const startCounter = createAction(
  CounterActionTypes.START_COUNTER,
);

export const pauseCounter = createAction(
  CounterActionTypes.PAUSE_COUNTER,
);

export const stopCounter = createAction(
  CounterActionTypes.STOP_COUNTER,
);

export const startTimer = createAction(
  CounterActionTypes.START_TIMER,
);
