export enum CounterActionTypes {
  INCREASE_COUNTER = '[Counter component] Increase counter',
  DECREASE_COUNTER = '[Counter component] Decrease counter',
  START_COUNTER = '[Counter component] Start counter',
  PAUSE_COUNTER = '[Counter component] Pause counter',
  STOP_COUNTER = '[Counter component] Stop counter',
  START_TIMER = '[Counter component] Start timer',
}
