import { Action, createReducer, on } from '@ngrx/store';
import * as CounterActions from './counter.actions';

export interface AppStoreStateInterface {
  counter?: CounterStateInterface,
}

export interface CounterStateInterface {
  firstNumber: number;
  secondNumber: number;
  isStarted: boolean;
  isPaused: boolean;
  isFirstStart: boolean;
}

export const initialState: CounterStateInterface = {
  firstNumber: 5,
  secondNumber: 10,
  isStarted: false,
  isPaused: false,
  isFirstStart: true
}

const counterReducer = createReducer(
  initialState,
  on(
    CounterActions.increaseCounter,
    (state: CounterStateInterface) => ({
      ...state,
      firstNumber: state.firstNumber + 1,
      secondNumber: state.secondNumber + 1
    })
  ),
  on(
    CounterActions.decreaseCounter,
    (state: CounterStateInterface) => ({
      ...state,
      firstNumber: state.firstNumber - 1,
      secondNumber: state.secondNumber - 1
    })
  ),
  on(
    CounterActions.startCounter,
    (state: CounterStateInterface) => ({
      ...state,
      isStarted: true,
      isPaused: false,
      isFirstStart: false
    })
  ),
  on(
    CounterActions.pauseCounter,
    (state: CounterStateInterface) => ({
      ...state,
      isStarted: false,
      isPaused: true
    })
  ),
  on(
    CounterActions.stopCounter,
    () => ({
      ...initialState
    })
  ),
)

export function reducer(state: CounterStateInterface, action: Action): CounterStateInterface {
  return counterReducer(state, action);
}
