import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStoreStateInterface, CounterStateInterface } from './counter.reducer';

export const counterSelector = createFeatureSelector<
  AppStoreStateInterface,
  CounterStateInterface
  >('counter');

export const firstNumberSelector = createSelector(
  counterSelector,
  (state: CounterStateInterface) => state.firstNumber
);

export const secondNumberSelector = createSelector(
  counterSelector,
  (state: CounterStateInterface) => state.secondNumber
);

export const isPausedSelector = createSelector(
  counterSelector,
  (state: CounterStateInterface) => state.isPaused
);

export const isStartedSelector = createSelector(
  counterSelector,
  (state: CounterStateInterface) => state.isStarted
);

export const isFirstStartSelector = createSelector(
  counterSelector,
  (state: CounterStateInterface) => state.isFirstStart
);


