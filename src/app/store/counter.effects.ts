import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import * as CounterActions from './counter.actions';
import { CounterService } from '../services/counter.service';
import { concatMap, switchMap, tap } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Store } from '@ngrx/store';

@Injectable()
export class CounterEffects {
  startCounter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CounterActions.startTimer),
      switchMap(() => {
        return this.message.loading('Delay before starting...', { nzDuration: 2500 })
          .onClose!.pipe(
          concatMap(() =>
            this.message.success('Start!', { nzDuration: 1000 }).onClose!),
          tap(() => {
            this.store.dispatch(CounterActions.startCounter());
            this.counterService.startCounter();
          }),
        );
      })
    ),
    { dispatch: false }
  )

  pauseCounter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CounterActions.pauseCounter),
      tap(() => this.counterService.stopCounter())
    ),
    { dispatch: false }
  );

  resetCounter$ = createEffect(() =>
      this.actions$.pipe(
        ofType(CounterActions.stopCounter),
        tap(() => this.counterService.stopCounter())
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private counterService: CounterService,
    private message: NzMessageService,
    private store: Store
  ) {}
}
