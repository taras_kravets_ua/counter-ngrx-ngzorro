import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { interval, Subject } from 'rxjs';
import * as CounterActions from '../store/counter.actions';
import { takeUntil, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  stopCounter$: Subject<boolean> = new Subject<boolean>();

  constructor(private store: Store) {
  }

  startCounter(): void {
    interval(1000).pipe(
      takeUntil(this.stopCounter$),
      tap(() => this.store.dispatch(CounterActions.increaseCounter()))
    ).subscribe();
  }

  stopCounter(): void {
    this.stopCounter$.next(true);
  }
}
