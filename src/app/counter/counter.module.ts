import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { CounterComponent } from './counter/counter.component';
import { CounterItemComponent } from './counter/counter-item/counter-item.component';
import { reducer } from '../store/counter.reducer';
import { CounterEffects } from '../store/counter.effects';


@NgModule({
  declarations: [
    CounterComponent,
    CounterItemComponent
  ],
  imports: [
    CommonModule,
    NzCardModule,
    NzIconModule,
    NzToolTipModule,
    NzTagModule,
    StoreModule.forFeature('counter', reducer),
    EffectsModule.forFeature([CounterEffects])
  ],
  exports: [
    CounterComponent
  ],
  bootstrap: [CounterComponent]
})
export class CounterModule { }
