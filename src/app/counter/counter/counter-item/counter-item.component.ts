import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-counter-item',
  templateUrl: './counter-item.component.html',
  styleUrls: ['./counter-item.component.scss']
})
export class CounterItemComponent {
  @Input() title: string = 'No title';
  @Input() number: number = 0;

  constructor() { }

}
