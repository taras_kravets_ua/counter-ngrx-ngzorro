import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as CounterActions from '../../store/counter.actions';
import { Observable } from 'rxjs';
import {
  firstNumberSelector,
  isPausedSelector,
  isStartedSelector,
  secondNumberSelector
} from '../../store/counter.selector';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  firstNumber$!: Observable<number>;
  secondNumber$!: Observable<number>;
  isPaused$!: Observable<boolean>;
  isStarted$!: Observable<boolean>;

  private isStarted = false;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.initializeValues();
  }

  increaseCounter() {
    this.store.dispatch(CounterActions.increaseCounter());
  }

  decreaseCounter() {
    this.store.dispatch(CounterActions.decreaseCounter());
  }

  startCounter() {
    if (!this.isStarted) {
      this.store.dispatch(CounterActions.startTimer());
    }
  }

  pauseCounter() {
    this.store.dispatch(CounterActions.pauseCounter());
  }

  stopCounter() {
    this.store.dispatch(CounterActions.stopCounter());
  }

  private initializeValues() {
    this.firstNumber$ = this.store.pipe(select(firstNumberSelector));
    this.secondNumber$ = this.store.pipe(select(secondNumberSelector));
    this.isPaused$ = this.store.pipe(select(isPausedSelector));
    this.isStarted$ = this.store.pipe(select(isStartedSelector));
    this.isStarted$.subscribe(isStarted => this.isStarted = isStarted);
  }
}
